const mongoose = require("mongoose");

const Connection = async () => {
    ATLAS_URL = process.env.DATABASE
    // ATLAS_URL = `mongodb+srv://${userName}:${Password}@cluster0.1ig8gsx.mongodb.net/Students?retryWrites=true&w=majority`
    try {
        await mongoose.connect(ATLAS_URL, {});
        console.log("Database connected successfully");
    } catch (error) {
        console.log("connection failed", error);
    }
}
module.exports = Connection;