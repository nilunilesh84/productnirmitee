const express = require('express')
const Connection = require('./Database/db')
var CronJob = require('cron').CronJob;
const dotenv = require("dotenv");
const userRouter = require('./Controller/UserController')
const notesRouter = require('./Controller/NotesController')
const leadRouter = require('./Controller/LeadController')
const app = express()
// const job = new CronJob('* * * * * *', function() {
// 	const d = new Date();
// 	console.log('Every Tenth Minute:', d);
// });
// console.log('After job instantiation');
// job.start();
const PORT = process.env.PORT || 8080
dotenv.config();
app.use(express.json());


app.use("/users", userRouter);
app.use("/leads", leadRouter)
app.use("/notes", notesRouter)



app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`)
    Connection()
})