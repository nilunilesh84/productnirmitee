const mongoose = require("mongoose");



const requiredformat = {
    type: String,
    required: true,
}

const LeadSchema = new mongoose.Schema({
    leadName: String,
    phoneNo: String,
    email: String,
    followUpDate: String,
    followUpTime: String,
    orgName: String,
    website: String,
    leadSource: String,
    products: String,
    activity: String,
    notes: String,
    dealAmount: String,
    jobTitle: String,
    labels: String,
    address: String,
    qualify: {
        type: Boolean,
        default: false
    },
    user: {
        ref: 'User',
        type: mongoose.Schema.Types.ObjectId
    }
}, { timestamps: true })

module.exports = mongoose.model('leads', LeadSchema)

