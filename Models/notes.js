const { Schema, model } = require('mongoose')

const notesSchema = new Schema({
    text: String,
    lead: {
        ref: 'leads',
        type: Schema.Types.ObjectId
    }
}, { timestamps: true });

module.exports = model('notes', notesSchema)