const express = require('express')
var admin = require("firebase-admin");
const app = express()
var serviceAccount = require("./nirmitee-7d0d3-firebase-adminsdk-i7bhy-c78b03f823.json");



const PORT = process.env.PORT || 4040
app.use(express.json());
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});



exports.sendNotificationToMembers = async (fcmToken) => {
     let deliveryResponse = await admin.messaging().send({
        token: fcmToken,
        data: {
            id: '1',
            subTitle: "subtitle"
        },
        android: {
            notification: {
                body: 'hello from node ',
                title: 'Leads created '
            }
        }
    }
    );
    return { ...deliveryResponse, };
}



// app.listen(PORT, () => {
//     console.log(`Server is running on ${PORT}`)
// })