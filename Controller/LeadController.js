const router = require('express').Router();
const Lead = require('../Models/LeadModel');
const Notes = require('../Models/notes');
const User = require('../Models/Model');
const { sendNotificationToMembers } = require('../app');

router.get('/', async (req, res) => {
    try {
        const data = await Lead.find()
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.post('/', async (req, res) => {
    try {
        const data = await Lead.create(req.body)
        await Notes.create({ text: req.body.notes, lead: data._id })
        const findUser = await User.findOne({ _id: req.body.user })
        console.log(findUser.fcmToken)
        sendNotificationToMembers(findUser.fcmToken)
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.get('/:id', async (req, res) => {
    try {
        const data = await Lead.find({ user: req.params.id })
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.put('/:id', async (req, res) => {
    try {
        const data = await Lead.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        const data = await Lead.deleteOne({ _id: req.params.id })
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

module.exports = router