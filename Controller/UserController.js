const router = require('express').Router();
const User = require('../Models/Model');
const bcrypt = require('bcrypt');
const { sendNotificationToMembers } = require('../app');

router.get('/', async (req, res) => {
    try {
        const data = await User.find()
        res.status(200).json({ data })
    } catch (error) {
        res.status(500).json("failed to get")
    }
})
router.post('/', async (req, res) => {
    const username = req.body.username
    const email = req.body.email
    const phoneNo = req.body.phoneNo
    const password = req.body.password
    try {
        const findDuplicate = await User.findOne({
            username
        })
        if (findDuplicate) {
            res.status(200).json(`User already exist with name ${username}`)
        } else {
            bcrypt.hash(password, 10, async function (err, hash) {
                if (err) {
                    res.status(500).json({ message: 'Something went wrong' })
                } else {
                    try {
                        const data = await User.create({
                            username,
                            email,
                            password: hash,
                            phoneNo
                        })
                        res.status(200).json({ message: 'User SignUp successfully', data })
                        console.log(data)
                    } catch (error) {
                        console.log(error)
                    }
                }
            });

        }
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})
const updateFcmToken = async (username, fcmToken) => {
    sendNotificationToMembers(fcmToken)
    const user = await User.findOneAndUpdate({ username }, { fcmToken }, { new: true });
    return user;
};

router.post('/login', async (req, res) => {
    const username = req.body.username
    const password = req.body.password
    const fcmToken = req.body.fcmToken
    try {
        const UserDetails = await User.findOne({ username })
        if (UserDetails) {
            bcrypt.compare(password, UserDetails.password, async function (err, result) {
                if (err) {
                    res.status(500).json({ message: 'Something went wrong' })
                } else {
                    if (result === true) {
                        const user = await updateFcmToken(username, fcmToken)
                        res.status(200).json({ message: 'User Login successfully', user })
                    } else {
                        res.status(500).json({ message: 'Wrong Password', err, result })
                    }
                }
            });
        } else {
            return res.status(404).json({ message: `User Not found with username ${username}` })
        }
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})
router.put('/:id', async (req, res) => {
    const _id = req.params.id
    try {
        const data = await User.findOneAndUpdate({ _id, }, req.body)
        res.status(200).json({ message: 'Updated successfully', data })
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})
router.delete('/:id', async (req, res) => {
    const id = req.params.id
    try {
        const data = await User.findByIdAndDelete({ _id: id })
        res.status(200).json({ message: 'Deleted Successfully', data })
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})


module.exports = router