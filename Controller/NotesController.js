const router = require('express').Router();
const Notes = require('../Models/notes')
const Lead = require('../Models/LeadModel')

router.get('/', async (req, res) => {
    try {
        const data = await Notes.find()
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.get('/:id', async (req, res) => {
    try {
        const data = await Notes.find({ lead: req.params.id })
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})


router.post('/', async (req, res) => {
    try {
        const data = await Notes.create(req.body)
        await Lead.findOneAndUpdate({ _id: req.body.lead }, { notes: req.body.text })
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

// router.get('/:id', async (req, res) => {
//     try {
//         const data = await Notes.findOne({ _id: req.params.id })
//         return res.status(200).json({ data })
//     } catch (error) {
//         return res.status(500).json({ error: error.message })
//     }
// })

router.put('/:id', async (req, res) => {
    try {
        const data = await Notes.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

router.delete('/:id', async (req, res) => {
    try {
        const data = await Notes.deleteOne({ _id: req.params.id })
        return res.status(200).json({ data })
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
})

module.exports = router